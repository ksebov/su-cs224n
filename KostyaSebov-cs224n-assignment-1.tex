\documentclass{article}
\usepackage{amsmath}
\usepackage{bbm}
\usepackage{ulem}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{listings}

\title{CS 224N: Assignment \#1}
\date{2017-01-26}
\author{Kostya Sebov}

\begin{document}
\newcommand*{\vertbar}{\rule[-1ex]{0.5pt}{2.5ex}}
\newcommand*{\horzbar}{\rule[.5ex]{2.5ex}{0.5pt}}

\maketitle

\paragraph{Question 1(a)}

\begin{equation*}
  softmax(x+c)_i =  \frac{e^{(x_i+c)}}{\sum_j{e^{(x_j+c)}}} 
  = \frac{e^{c}e^{x_i}}{\sum_j{e^{c}e^{x_j}}}
  = \frac{e^{c}e^x_i}{e^c\sum_j{e^{x_j}}}
  = \frac{e^x_i}{\sum_j{e^{x_j}}}
  = softmax(x)_i 
\end{equation*}

\paragraph{Question 2(a)}

\begin{align}
  \frac{d}{dx} \sigma(x) 
&= \frac{d}{dx}\frac{1}{1+e^x} 
  = \frac{-1}{(1+e^x)^2}\frac{d}{dx}(1+e^x)
  = \frac{-e^x}{(1+e^x)^2} \\
&= \frac{1}{1+e^x} \frac{-e^x}{1+e^x} 
  = \sigma(x) \frac{({1+e^x})-({1+e^x})-e^x}{1+e^x} \\
 &= \sigma(x) \left( 1- \frac{1}{1+e^x}\right) = \sigma(x) (1-\sigma(x)) \label{sigma_prime}
\end{align}

\paragraph{Question 2(b)}

\begin{align*}
\forall k : \frac{\partial}{\partial \theta_k} log(\hat{y_i}) 
&= \frac{\partial}{\partial \theta_k} log \left( \frac{e^{\theta_i}}{\sum_j{e^{\theta_j}}}\right)\\
&= \frac{\partial}{\partial \theta_k}log(e^{\theta_i}) - \frac{\partial}{\partial \theta_k}log \left(\sum_j{e^{\theta_j}}\right)\\
&= \frac{\partial \theta_i}{\partial \theta_k}- \frac{1}{\sum_j{e^{\theta_j}}}\frac{\partial}{\partial \theta_k}\sum_j{e^{\theta_j}}\\
&= \mathbbm{1}\{i=k\} - \frac{e^{\theta_k}}{\sum_j{e^{\theta_j}}} 
  = \mathbbm{1}\{i=k\} - \hat{y_k} \label{sigmoid_prime}
\end{align*}

\begin{align*}
\frac{\partial}{\partial \theta_k} CE(y,\hat{y}) 
&= -\sum_i{y_i \frac{\partial}{\partial \theta_k}log(\hat{y_i})} = - y_k(1-\hat{y_k}) + \sum_{i\neq k}{y_i\hat{y_k}} \\
&= -y_k + y_k\hat{y_k} +  \sum_{i\neq k}{y_i\hat{y_k}} = \hat{y_k}\sum_i{y_i} - y_k
\end{align*}

Since $y$ is a one-hot label vector, $\sum_i{y_i} = 1$, hence we can conclude that 

\begin{equation*}
\frac{\partial}{\partial \theta_k} CE(y,\hat{y}) = \hat{y_k} - y_k
\end{equation*}

...or, in vector form
\begin{equation}\label{dCEdtheta}
\frac{\partial}{\partial \theta} CE(y,\hat{y}) = \hat{y} - y
\end{equation}

Note that $\theta$, the gradient, $y$ and $\hat{y}$ can be either row or column vectors as long as they all have the same shape.

\paragraph{Question 2(c)}

Let us first define some variables:

\begin{align}
z &= x\boldsymbol{W_1} + \boldsymbol{b_1} \label{z}\\
h &= sigmoid(z)\\
\theta &= h\boldsymbol{W_2} + \boldsymbol{b_2} \label{theta} \\
\hat{y} &= softmax(\theta)\\
J &= CE(y,\hat{y})
\end{align}


\noindent For constency with programming assignment, we assume $x$, $h$, and $y$ are all row vectors having dimensions $(1\times D_x)$ $(1\times H)$ $(1\times D_y)$ respectively.
Since $\boldsymbol{b_1}$ and $z$ are of the same shape as $h$, they are all $(1\times H)$ vectors. 
Since $\boldsymbol{b_2}$, $\theta$ and $\hat{y}$ are of the same shape as $y$, they are $(1\times D_y)$ vectors. 
For \eqref{z} to be a valid equation, $\boldsymbol{W_1}$ must be a $(D_x\times H)$ matrix.
Similarly, because of \eqref{theta}, $\boldsymbol{W_2}$ must be a $(H\times D_y)$ matrix.

\begin{table}[h!]
  \centering
  \caption{Variable and matrix dimensions.}
  \label{tab:dims}
  \begin{tabular}{ccccc}
    \toprule
    $x$  & $\boldsymbol{W_1}$ & $h$, $z$, $\boldsymbol{b_1}$ & $\boldsymbol{W_2}$ & $y$, $\hat{y}$, $\theta$, $\boldsymbol{b_2}$\\
    \midrule
    $(1\times D_x)$ & $(D_x\times H)$ & $(1\times H)$ & $(H\times D_y)$ & $(1\times D_y)$\\
    \bottomrule
  \end{tabular}
\end{table}

\noindent Using chain rule and simple equation partial derivative formulas we can calculate the following composite derivative:

\begin{align}
\frac{\partial J}{\partial x} = \frac{\partial J}{\partial \theta} \frac{\partial \theta}{\partial h} \frac{\partial h}{\partial z}\frac{\partial z}{\partial x}
= &(\hat{y} - y)\boldsymbol{W_2}^\top diag\{\sigma'(z)\} \boldsymbol{W_1}^\top \label{dJ_dx}
\end{align}

The following table verifies dimensionality correctness of the above equation:

\begin{table}[h!]
  \centering
  \label{tab:table1}
  \begin{tabular}{ccccc}
    \toprule
    $\frac{\partial J}{\partial x}$ & $(\hat{y} - y)$ & $\boldsymbol{W_2}^\top$ & $diag\{\sigma'(z)\}$ & $\boldsymbol{W_1}^\top$\\
    \midrule
    $(1\times D_x)$ & $(1\times D_y)$ & $(D_y\times H)$ & $(H\times H)$ & $(H\times D_x)$\\
    \bottomrule
  \end{tabular}
\end{table}	

Furthermore, forgoing matrix product assotiavity, using $\odot$ to denote Hadamard product and taking into account \eqref{sigma_prime}, \eqref{dJ_dx} can be optimized to:

\begin{align}
\frac{\partial J}{\partial x} 
= &(((\hat{y} - y)\boldsymbol{W_2}^\top) \odot (1-z)\odot z)\boldsymbol{W_1}^\top \label{dJ_dx}
\end{align}

\paragraph{Question 2(d)}

Assuming dimesionalities of $\boldsymbol{W_1}$, $\boldsymbol{b_1}$, $\boldsymbol{W_2}$ and $\boldsymbol{b_2}$  (see Table \ref{tab:dims}) the total number of parameters is 
$$
D_x H + H + H D_y + D_y
$$

\paragraph{Questions 3(a) and 3(b)}

For consistency with programming assignmend we assume $u_i$ and $v_i$ are $(1\times D)$ row vectors. Let us first define:
\begin{align}
U &= 
\left(
  \begin{array}{ccc}
    \text{---} & u_1 & \text{---} \\
    \text{---} & u_2 & \text{---}  \\
                & \vdots &   \\
    \text{---} & u_W & \text{---}  
  \end{array}
\right) &\in \mathbbm{R}^{(W\times D)}\\
\theta &= \left( u_1\cdot v_c, u_2\cdot v_c ... u_W\cdot v_c \right) = v_c U^\top = (U v_c^\top)^\top  &\in \mathbbm{R}^{(1\times W)}\\ \\
\hat{y} &= softmax(\theta) &\in \mathbbm{R}^{(1\times W)}\\ \\
y(o) &\text{ is a one-hot label row vector} &\in \mathbbm{R}^{(1\times W)}  
\end{align}

Then, assuming $J_{softmax-CE}(o, v_c, U) = CE(y, \hat{y})$ and using \eqref{dCEdtheta}, we can calculate the following derivatives:
\begin{align}
\frac{\partial J}{\partial v_c} &= \frac{\partial J}{\partial \theta} \frac{\partial \theta}{\partial v_c} = (\hat{y} - y)U \label{DJsmceDvc} \\
\frac{\partial J}{\partial U} &= \frac{\partial J}{\partial \theta} \frac{\partial \theta}{\partial U} = (\hat{y} - y)^\top \label{DJsmceDu} v_c
\end{align}

The following table verifies dimensionality correctness of the above equations:

\begin{table}[h!]
  \centering
  \begin{tabular}{ccccccc}
    \toprule
    $\frac{\partial J}{\partial v_c}$ & $(\hat{y} - y)$ & $U$ & & $\frac{\partial J}{\partial U}$ & $(\hat{y} - y)^\top$ & $v_c$\\
    \midrule
    $(1\times D)$ & $(1\times W)$ & $(W\times D)$ & & $(W\times D)$ & $(W\times 1)$ & $(1\times D)$ \\
    \bottomrule
  \end{tabular}
\end{table}	


\paragraph{Question 3(c)}

For consistency with programming assignmend we assume $u_i$ and $v_i$ are $(1\times D)$ row vectors. Let us first define:
\begin{align*}
U^{(K)} &= 
\left(
  \begin{array}{ccc}
    \text{---} & u_o & \text{---} \\
    \text{---} & u_1 & \text{---} \\
    \text{---} & u_2 & \text{---}  \\
                & \vdots &   \\
    \text{---} & u_K & \text{---}  
  \end{array}
\right) &\in \mathbbm{R}^{(K+1)\times D}\\ \\
\delta &= \left( \sigma(u_o\cdot v_c)-1, \sigma(u_1\cdot v_c), \sigma(u_2\cdot v_c) ... \sigma(u_K\cdot v_c) \right) = (U^{(K)} v_c^\top)^\top - (1, 0 ... 0) &\in \mathbbm{R}^{1\times (K+1)}
\end{align*}

Also, let's observe the following reltationship:
\begin{align*}
\frac{\partial}{\partial a} log( \sigma(a \cdot b)) 
&= \frac{1}{\sigma(a \cdot b)}\frac{\partial}{\partial a} \sigma(a\cdot b)
  = \frac{\sigma(a \cdot b)(1-\sigma(a \cdot b))}{\sigma(a \cdot b)}\frac{\partial}{\partial a} (a\cdot b)
  = (1-\sigma(a \cdot b))b
\end{align*}


From the above and the definition of $J_{neg.s.}$:
\begin{align}
J_{neg.s.}(0, v_c, U) 
&= -log(\sigma(u_o\cdot v_c)) - \sum_{k=1}^K{log(\sigma(-u_k \cdot v_c))} \label{Jns}
\end{align}

follows:

\begin{align}
\begin{split}
\frac{\partial J_{neg.s.}}{\partial v_c} 
&= -(1-\sigma(u_o\cdot v_c)) u_o - \sum_{k=1}^K{(1-\sigma(-u_k \cdot v_c))(-u_k)} \\
&= (\sigma(u_o\cdot v_c)-1)u_o + \sum_{k=1}^K{\sigma(u_k \cdot v_c)u_k}\\
&= -u_o + \sum_{k=o}^K{\sigma(u_k \cdot v_c)u_k} = \delta U^{(K)} \label{DJnsDvc}\\
\end{split}
\end{align}

Simlarly:

\begin{align*}
\frac{\partial J_{neg.s.}}{\partial u_o}
&= -(1-\sigma(u_o\cdot v_c)) v_c  = v_c (\sigma(u_o\cdot v_c)-1)\\
\frac{\partial J_{neg.s.}}{\partial u_k}
&= +(1-\sigma(-u_k\cdot v_c)) v_c  = v_c\sigma(u_k\cdot v_c)
\end{align*}

or in matrix form:
\begin{align}
\frac{\partial J_{neg.s.}}{\partial U^{(K)}} = \delta^\top v_c \label{DJnsDu}
\end{align}

Note that since $U^{(K)}$ is a submatrix of the whole output word matrix $U$. Therefore $\frac{\partial J_{neg.s.}}{\partial U}$ is a sparse matrix with zeros everywhere except in corresponding rows from $\frac{\partial J_{neg.s.}}{\partial U^{(K)}}$.

The cost of computing \eqref{Jns} \eqref{DJnsDvc} and \eqref{DJnsDu} is $O(KD)$. On the other hand, the cost of computing $J_{softmax-CE}$ is $O(WD)$, which makes it roughly $W/D$ more expensive. Since W runs in thousands or tens of thounsands while K runs in tens, negative sampling is aproximately two to three orders of magnitude more efficient.

\paragraph{Question 3(c)}

Since every each member of skip-gram cost sum is dependent on its all output words in the dictionary as well as on the center predicted word:
\begin{align*}
\frac{\partial J_{skip-gram}}{\partial U} &=  \sum_{-m\leq j\leq m;j\neq0}\frac{\partial F(w_{c+j}, v_c)}{\partial U}\\
\frac{\partial J_{skip-gram}}{\partial v_c} &= \sum_{-m\leq j\leq m;j\neq0}\frac{\partial F(w_{c+j}, v_c)}{\partial v_c}
\end{align*}

COWB's sincle member is in addition dependent on its all constuents of $\hat{v}$:
\begin{align*}
\frac{\partial J_{CBOW}}{\partial U} &=  \frac{\partial F(w_c, \hat{v})}{\partial U}\\
\frac{\partial J_{CBOW}}{\partial v_i} &= \frac{\partial F(w_c, \hat{v})}{\partial \hat{v}} \frac{\partial \hat{v}}{\partial v_i}
= 
\left\{
  \begin{array}{cc}
    \frac{\partial F(w_c, \hat{v})}{\partial \hat{v}} & \forall i \in \{w_{c-m}...w_{c-1},w_{c+1}...w_{c+m}\} \\
    0 & \text{othewise}
  \end{array}
\right\}\\
\end{align*}

\paragraph{Question 3(g)} 
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{assignment1/q3_word_vectors.png}
  \caption{Training result visualisation.}
  \label{fig:word_vecs}
\end{figure}
The training result visualisation is shown on Figure \ref{fig:word_vecs}. It appears a number of words expressing human satisfaction or degree of enjoyment are clustering together. On the other hand, we can see that both negative and positive sentiments appear mixed. It is possible that this is a limitation of 2D projection and the the words corresponding to the oposite sentiments but appearing together in this plot, e.g. great and waste, are more distant in other dimensions.



\paragraph{Question 4(b)}
Regularization is one of the methods used to prevemt overfitting of the model that is to prevent the model from memorizing the training data set rather than learning its general features that would be applicable to more data. Although there are other methods, regularization is a simple, quantifiable parameter that imposes penalty on mode's numeric complexity, i.e. overall magnitude of its parameters.

\paragraph{Question 4(c)}
\begin{verbatim}
def chooseBestModel(results):

    ### YOUR CODE HERE
    bestResult = results[0]

    for model in results[1:]:
        if model["dev"] > bestResult["dev"]:
            bestResult = model

    ### END YOUR CODE

    return bestResult
\end{verbatim}

\paragraph{Question 4(d)}
The following table summarizes the best accuracies from 2 respective runs of \verb|q4 sentiment.py|:

\begin{table}[h!]
  \centering
  \label{tab:table1}
  \begin{tabular}{lcccc}
    \toprule
    Model &  & Train & Dev & Test\\
    \midrule
    My word vectors from q3                     &  & 30.489 & 31.244 & 30.136\\
    Pretrained GloVe vectors on Wikipedia &  & 39.162 & 37.148 & 38.054\\
    \bottomrule
  \end{tabular}
\end{table}	

Clearly GloVe model produced better results. The most likely reasons for it are (1) higher dimensionality of the model allowing for richer feature representation, (2) significanly greater amount of training data producing that allows capturing more detailed relationships and, perhaps, (3) the ability of GloVe model to minimize the effect of words and word pairs that occur much more often.

\paragraph{Question 4(e)} The plot of the train and dev accuracy with respect to the regularization value generated by \verb|q4_sentiment.py --pretrained| is shown on Figure \ref{fig:acc_vs_lambda}.

\begin{figure}[h!]
  \includegraphics[width=\linewidth]{assignment1/q4_reg_v_acc.png}
  \caption{Accuracy vs regularization parameter.}
  \label{fig:acc_vs_lambda}
\end{figure}

The plot clearly demonstrates the effect of regularization parameter on model behavior. Since this is a relatively simple sentence model with word relationship features we can see that the increased regularization has little effect on training until  the parameter gets huge, which time model practically collapses. Also the simplicity of model leads to its slow generalization as the gap between train and dev accuracies stays almost constant for long time. Eventually, at lambda's sweet spot the dev accuracy does rises and comes much closer to the training one giving us a warm and fuzzy feeling that the model reached its best.

\paragraph{Question 4(e)} 

\begin{figure}[h!]
  \includegraphics[width=\linewidth]{assignment1/q4_dev_conf.png}
  \caption{Confusion matrix.}
  \label{fig:conf_matrix}
\end{figure}

The confusion matrix generated by \verb|q4_sentiment.py --pretrained| is shown on Figure \ref{fig:conf_matrix}.

The biggest problem demonstrated by this matrix is that the model fails to be precise. It's generally correct in finding general mood of the sentence, however it's to shy to be precise-- neither extremes, nor neutral buckets are aqequately represented. It also appears the "modest" +/- demonstrate some kind of bell curve pattern pointing to mostly random (Gausian?) distribution indicating unsophisticated nature of the sentence model.

\pagebreak
\paragraph{Question 4(f)} The following are the exempliary mistakes the model made:

\begin{verbatim}
3	1	and if you 're not nearly moved to tears by a couple of scenes , 
you 've got ice water in your veins .
\end{verbatim}
A clearly positive sentence was mistakenly predicted as a very negative because it failed to capture the sarcasm of the reviewer

\begin{verbatim}
3	1	nothing 's at stake , just a twisty double-cross you can smell a 
mile away -- still , the derivative nine queens is lots of fun .|
\end{verbatim}

A similar misclassification, most likely caused by many negative words like "nothing", "twisty" or "double-cross", even thought a "lots of fun" at the end would defintely otweghted the decision if the feature model accounted for it.

\begin{verbatim}
0	3	ultimately feels empty and unsatisfying , like swallowing a 
communion wafer without the wine .
\end{verbatim}

An opposite mistake, quite a puzzling one because "unsatisfying" and "empty" should have put it in the grib bucket. A bug? 


\end{document}


