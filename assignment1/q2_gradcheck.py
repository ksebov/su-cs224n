#!/usr/bin/env python

import numpy as np
import random
import q1_softmax
import q2_sigmoid

# First implement a gradient checker by filling in the following functions
def gradcheck_naive(f, x):
    """ Gradient check for a function f.

    Arguments:
    f -- a function that takes a single argument and outputs the
         cost and its gradients
    x -- the point (numpy array) to check the gradient at
    """

    rndstate = random.getstate()
    random.setstate(rndstate)
    fx, grad = f(x) # Evaluate function value at original point
    h = 1e-4        # Do not change this!

    # Iterate over all indexes in x
    it = np.nditer(x, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:
        ix = it.multi_index

        # Try modifying x[ix] with h defined above to compute
        # numerical gradients. Make sure you call random.setstate(rndstate)
        # before calling f(x) each time. This will make it possible
        # to test cost functions with built in randomness later.

        x0 = x.copy()
        x0[ix] -= h
        random.setstate(rndstate)
        fx0 = f(x0)[0] # Evaluate function value at original point

        x1 = x.copy()
        x1[ix] += h
        random.setstate(rndstate)
        fx1 = f(x1)[0] # Evaluate function value at original point

        numgrad = (fx1-fx0)/2/h

        # Compare gradients
        reldiff = abs(numgrad - grad[ix]) / max(1, abs(numgrad), abs(grad[ix]))
        if reldiff > 1e-5:
            print "Gradient check failed."
            print "First gradient error found at index %s" % str(ix)
            print "Your gradient: %f \t Numerical gradient: %f" % (
                grad[ix], numgrad)
            return False

        it.iternext() # Step to next dimension

    print "Gradient check passed!"
    return True


def sanity_check():
    """
    Some basic sanity checks.
    """
    quad = lambda x: (np.sum(x ** 2), x * 2)

    print "Running sanity checks..."
    gradcheck_naive(quad, np.array(123.456))      # scalar test
    gradcheck_naive(quad, np.random.randn(3,))    # 1-D test
    gradcheck_naive(quad, np.random.randn(4,5))   # 2-D test
    print ""


def sygmoid_and_grad(x):
    s = q2_sigmoid.sigmoid(x)

    return (s, q2_sigmoid.sigmoid_grad(s))


def your_sanity_checks():
    """
    Use this space add any additional sanity checks by running:
        python q2_gradcheck.py
    This function will not be called by the autograder, nor will
    your additional tests be graded.
    """
    print "Running your sanity checks..."

    print "Running sanity checks..."
    gradcheck_naive(sygmoid_and_grad, np.array(123.456))      # scalar test


if __name__ == "__main__":
    sanity_check()
    your_sanity_checks()
